Title: Usability requirements for GNU/Linux smartphones
Date: 2010-12-03 10:20
Category: Article

# Introduction

Having participated in the port of GNU/Linux to Android devices, and to the
creation of an Android distribution, I am aware of some of the impact of
hardware design decicions on usability, and substainability.

# Usability requirements for GNU/Linux smartphones

*   Smartphones have small screens with high
    [pixel density](https://en.wikipedia.org/wiki/Pixel_density).
    The consequence is that the
    [desktop environment](https://en.wikipedia.org/wiki/Desktop_environment) has
    to be adapted to it, for instance by making it practical to close applications.
    A typical issue is when the close button is so tiny (and in the right corner
    of the screen) that the user needs a lot of trials and effort to actually
    succeed in clicking it.
    The desktop environment can and GNU/Linux distribution can also help by
    shipping configurations that makes graphical applications more friendly to a
    smartphone display.
    
    This can be done by modifying gtkrc and other similar configuration files.
    However integrating properly all the applications available in a GNU/Linux
    distribution is a very big task, so to cope with such issue, some smartphones
    targetting GNU/Linux (Nokia N900, Openmoko GTA01 and GTA02, Goldelico GTA04) use
    a resistive touchscreen, for their precision.
    Practically speaking it permits to use less well adapted applications too.
      
* The desktop environment or applications should not require hardware 3D
acceleration.
This is because, currently, few of the smartphones GPUs are well supported by
free software.
The devices that do have free software 3D acceleration often have various other
freedom issues (like a signed bootloader).
Most if not all of the smartphones that don't have signed bootloaders have GPUs
that are not supported by free software.

* Having a hardware keyboard also helps with application compatibility such as
games.
* The desktop manager needs to make sure that the users are able to quit any
application easily (not misss calls for instance): That can arrise if they tried
to play a 3D game that they didn't manage to close

# Usability requirements for Android smartphones:

* The android GUI and framework has been designed to be usable with (only) (big)
fingers. Since the capacitive touchscreen works well with fingers, and that
resistive touchscreen tend not to (They often requires a stylus), having a
capacitive touchscreen is more convenient as it doesn't require the user to use
a stylus.

# Substainability requirement for mobile devices.

* The device needs to have very good support in mainline Linux.
It must be usable with a mainline Linux kernel as-is. If it isn't, the
device will soon be useless.
If users can't easily upgrade the kenrel, they will not be able to run
most of the recent GNU/Linux (or Android) distributions.
Such distributions uses systemd which depends on features only found in recent
kenrel versions.
Android can now be booted with very few changes to the mainline linux kenrel.

* Some devices requires a working battery to be able to boot, and to charge that
battery, they need to be able to boot up to the point where software enables
the charge of that battery. This should be avoided.
