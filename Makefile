DESTDIR ?= output

include pelican/Makefile
BASEDIR := pelican
OUTPUTDIR := $(DESTDIR)/pelican/output

STATIC_FILES := \
	COPYING \
	articles/boot_trustworthyness.css \
	articles/boot_trustworthyness.html \
	articles/briqueinter.net.txt \
	articles/free_software_contributions.css \
	articles/free_software_contributions.html \
	articles/index.html \
	boxinter.net.txt \
	briqueinter.net.txt \
	contact.html \
	dns/wrt54gsv4 \
	downloads/README.txt \
	downloads/gpg.txt \
	git.html \
	index.html \
	mirrors/opensource.samsung.com/COPYING.AGPLv3 \
	mirrors/opensource.samsung.com/checksums/GT-N7105.md5 \
	mirrors/opensource.samsung.com/checksums/GT-N7105.sha1 \
	mirrors/opensource.samsung.com/checksums/GT-N7105.sha256 \
	mirrors/opensource.samsung.com/checksums/GT-N7105.sha512 \
	mirrors/opensource.samsung.com/gen_table.py \
	mirrors/opensource.samsung.com/index.html \
	setup/site.html \
	wikis.html \

install: html
	@for file in $(STATIC_FILES) ; do \
		install -m755 -d $(DESTDIR)/`dirname $$file` ; \
		install -m644 $$file $(DESTDIR)/$$file ; \
	done
