#!/usr/bin/env python
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os

def generate_table_entry(device, version, notice, zip_file):
    table_entry = """
  <tr>
    <th>{device}</th>
    <th>{version}</th>
    <th>{zip_file}</th>
    <th>
      <a href=\"{device}/{version}/{zip_file}\">
        Source
      </a>
    <th>
      <a href=\"{device}/{version}/{notice}\">
        Announcement
      </a>
    </th>
  </tr>""".format(device=device,
                  version=version,
                  notice=notice,
                  zip_file=zip_file)
    return table_entry

# From the website
# GT-N7105 	N7105XXUFQH1 	GT-N7105_EUR_KK_Opensource.zip
# GT-N7105 	N7105XXUFND3 	GT-N7105_KK_Opensource.zip
# GT-N7105 	N7105XXUEMK9 	GT-N7105_JB_Opensource.zip
# GT-N7105 	N7105XXDMB6 	GT-N7105_JB_Opensource.zip
# GT-N7105 	N7105XXALID 	GT-N7105_JB_Opensource.zip

device = 'GT-N7105'
versions = os.listdir(device)
for version in versions:
    files = os.listdir(device + os.sep + version)
    files.remove('NOTICE.html')
    if len(files) == 0:
        entry = generate_table_entry(device, version, 'NOTICE.html', 'TODO')
    elif len(files) == 1:
        entry = generate_table_entry(device, version, 'NOTICE.html', files[0])
        print(entry)
